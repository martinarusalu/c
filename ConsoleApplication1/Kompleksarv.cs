﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Kompleksarv
    {
        private int _reaalosa;
        private int _imaginaarosa;

        public Kompleksarv (int reaalosa, int imaginaarosa)
        {
            _reaalosa = reaalosa;
            _imaginaarosa = imaginaarosa;
        }

        public override string ToString()
        {
            return _reaalosa.ToString() + (_imaginaarosa > 0 ? " + " : " - ") +  _imaginaarosa.ToString() + "i";
        }

        public int Imaginaarosa
        {
            get { return _imaginaarosa; }
            set { _imaginaarosa = value; }
        }

        public int Reaalosa
        {
            get { return _reaalosa; }
            set { _reaalosa = value; }
        }

        public Kompleksarv liida(Kompleksarv k1)
        {
            int reaalosa = _reaalosa += k1.Reaalosa;
            int imaginaarosa = _imaginaarosa += k1.Imaginaarosa;
            return new Kompleksarv(reaalosa, imaginaarosa);
        }

        public Kompleksarv lahuta(Kompleksarv k1)
        {
            int reaalosa = _reaalosa -= k1.Reaalosa;
            int imaginaarosa = _imaginaarosa -= k1.Imaginaarosa;
            return new Kompleksarv(reaalosa, imaginaarosa);
        }
    }
}
