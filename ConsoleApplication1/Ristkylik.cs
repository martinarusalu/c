﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Ristkylik
    {
        private int _pikkus;
        private int _laius;

        public Ristkylik(int pikkus, int laius)
        {
            _pikkus = pikkus;
            _laius = laius;
        }

        public int arvutaPindala()
        {
            return _pikkus * _laius;
        }

        public int arvutaYmbermoot()
        {
            return (_pikkus + _laius) * 2;
        }

        public bool kasOnVordsed(Ristkylik r1, Ristkylik r2)
        {
            return (r1.arvutaPindala() == r2.arvutaPindala()) && (r1.arvutaYmbermoot() == r2.arvutaYmbermoot());
        }

        public bool kasOnRuut()
        {
            return _laius == _pikkus;
        }
    }
}
