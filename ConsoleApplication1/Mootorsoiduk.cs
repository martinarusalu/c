﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Mootorsoiduk
    {
        private int _kiirus;
        private int _maxKiirus;

        public override string ToString()
        {
            return hetkeseis();
        }

        public Mootorsoiduk(int maxKiirus = 20)
        {
            _kiirus = 0;
            _maxKiirus = maxKiirus;
        }

        public void kiirenda()
        {
            kiirenda(10);
        }

        public virtual void kiirenda(int kiirendus)
        {
            _kiirus = (_kiirus + 10) > _maxKiirus ? _maxKiirus : _kiirus + kiirendus;
        }

        public void stop()
        {
            _kiirus = 0;
        }

        public virtual string hetkeseis()
        {
            return _kiirus != 0 ? "Sõidab kiirusega " + _kiirus + " km/h" : "Seisab";
        }
    }
}
