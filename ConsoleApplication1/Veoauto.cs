﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Veoauto : Auto
    {
        private bool _kallutab;

        public override string ToString()
        {
            return hetkeseis();
        }

        public Veoauto(int maxKiirus = 70) : base(maxKiirus)
        {
            _kallutab = false;
        }

        public void hakkaKallutama()
        {
            base.stop();
            _kallutab = true;
        }

        public override void kiirenda(int kiirendus)
        {
            if (!_kallutab) base.kiirenda(kiirendus);
        }

        public override string hetkeseis()
        {
            return base.ToString() + (_kallutab ? ", kallutab" : ", ei kalluta");
        }
    }
}
