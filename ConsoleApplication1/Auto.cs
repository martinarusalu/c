﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Auto : Mootorsoiduk
    {
        private bool _uksedAvatud;

        public Auto(int maxKiirus = 100) : base(maxKiirus)
        {
            _uksedAvatud = false;
        }

        public void avaUksed()
        {
            stop();
            _uksedAvatud = true;
        }

        public void sulgeUksed()
        {
            _uksedAvatud = false;
        }

        public override void kiirenda(int kiirendus)
        {
            if (!_uksedAvatud) base.kiirenda(kiirendus);
        }

        public override string hetkeseis()
        {
            return base.ToString() + (_uksedAvatud ? ", uksed avatud" : ", uksed suletud");
        }

        public override string ToString()
        {
            return hetkeseis();
        }
    }
}
