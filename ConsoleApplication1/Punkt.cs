﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConsoleApplication1
{
    class Punkt
    {
        private int _x;
        private int _y;

        public Punkt(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public int X
        {
            get { return _x; }
            set { _x = value; }
        }

        public int Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public double kaugusNullist()
        {
            int x = Math.Abs(_x);
            int y = Math.Abs(_y);
            return (double) Math.Sqrt(x*x+y*y);
        }

        public void teataAndmed()
        {
            Console.WriteLine(String.Format("({0},{1})",_x,_y));
        }

        public double kaugusTeisestPunktist(Punkt p1)
        {
            int x = Math.Abs(_x - p1.X);
            int y = Math.Abs(_y - p1.Y);
            return (double)Math.Sqrt(x * x + y * y);
        }

        public bool kasOnAlgusPunkt()
        {
            return (_x == 0) && (_y == 0);
        }

    }
}
