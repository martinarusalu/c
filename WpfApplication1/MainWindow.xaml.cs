﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Inimene> _list = new List<Inimene>();
        public MainWindow()
        {
            InitializeComponent();
            lstInimesed.ItemsSource = _list;
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            if (lstInimesed.SelectedIndex > -1)
            {
                Inimene inimene = lstInimesed.SelectedItems as Inimene;
                if (inimene != null)
                {
                    inimene.Eesnimi = txtEesnimi.Text;
                    inimene.Perenimi = txtPerenimi.Text;
                    inimene.Pikkus = int.Parse(txtPikkus.Text);
                    lstInimesed.SelectedIndex = -1;
                }
            }
            else
            {
                Inimene inimene = new Inimene(txtEesnimi.Text, txtPerenimi.Text, int.Parse(txtPikkus.Text));
                _list.Add(inimene);
                lstInimesed.Items.Refresh();
                txtEesnimi.Clear();
                txtPerenimi.Clear();
                txtPikkus.Clear();
            }
        }

        private void lstInimesed_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lstInimesed.SelectedIndex > -1)
            {
                Inimene inimene = lstInimesed.SelectedItem as Inimene;
                if (inimene != null)
                {
                    txtEesnimi.Text = inimene.Eesnimi;
                    txtPerenimi.Text = inimene.Perenimi;
                    txtPikkus.Text = inimene.Pikkus.ToString();
                    btnLisa.Content = "Muuda";
                }
            }
        }
    }
}
